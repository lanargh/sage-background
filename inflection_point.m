function op2=inflection_point(s, min_value, max_value)
    sections2 = s(min_value:max_value);
    diff_value = abs(diff(sections2,2));
    [min_diff_value,locs] = min(diff_value);
    op2 = min_value+locs;
