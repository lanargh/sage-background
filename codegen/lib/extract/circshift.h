/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * circshift.h
 *
 * Code generation for function 'circshift'
 *
 */

#ifndef CIRCSHIFT_H
#define CIRCSHIFT_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern void circshift(emxArray_real_T *a, double p);

#endif

/* End of code generation (circshift.h) */
