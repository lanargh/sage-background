/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * findpeaks.c
 *
 * Code generation for function 'findpeaks'
 *
 */

/* Include files */
#include "findpeaks.h"
#include "eml_setop.h"
#include "extract.h"
#include "extract_emxutil.h"
#include "rt_nonfinite.h"
#include "sort.h"
#include <math.h>

/* Function Definitions */
void findpeaks(const emxArray_real_T *Yin, emxArray_real_T *Ypk, emxArray_real_T
               *Xpk)
{
  emxArray_uint32_T *x;
  int i;
  int kfirst;
  emxArray_int32_T *idx;
  emxArray_int32_T *iInfinite;
  emxArray_int32_T *b_sortIdx;
  int ny;
  int nPk;
  int nInf;
  char dir;
  double ykfirst;
  boolean_T isinfykfirst;
  int k;
  emxArray_int32_T *iPk;
  double yk;
  boolean_T isinfyk;
  int i1;
  char previousdir;
  emxArray_int32_T *c;
  int n;
  emxArray_int32_T *iwork;
  int b_i;
  emxArray_uint32_T *locs_temp;
  int q;
  int qEnd;
  emxArray_boolean_T *idelete;
  int kEnd;
  emxArray_boolean_T *r;
  emxArray_int32_T *r1;
  emxInit_uint32_T(&x, 1);
  i = x->size[0];
  x->size[0] = Yin->size[0];
  emxEnsureCapacity_uint32_T(x, i);
  kfirst = Yin->size[0] - 1;
  for (i = 0; i <= kfirst; i++) {
    x->data[i] = i + 1U;
  }

  emxInit_int32_T(&idx, 1);
  emxInit_int32_T(&iInfinite, 1);
  emxInit_int32_T(&b_sortIdx, 1);
  i = idx->size[0];
  idx->size[0] = Yin->size[0];
  emxEnsureCapacity_int32_T(idx, i);
  i = iInfinite->size[0];
  iInfinite->size[0] = Yin->size[0];
  emxEnsureCapacity_int32_T(iInfinite, i);
  ny = Yin->size[0];
  nPk = 0;
  nInf = 0;
  dir = 'n';
  kfirst = 0;
  ykfirst = rtInf;
  isinfykfirst = true;
  for (k = 1; k <= ny; k++) {
    yk = Yin->data[k - 1];
    if (rtIsNaN(yk)) {
      yk = rtInf;
      isinfyk = true;
    } else if (rtIsInf(yk) && (yk > 0.0)) {
      isinfyk = true;
      nInf++;
      iInfinite->data[nInf - 1] = k;
    } else {
      isinfyk = false;
    }

    if (yk != ykfirst) {
      previousdir = dir;
      if (isinfyk || isinfykfirst) {
        dir = 'n';
      } else if (yk < ykfirst) {
        dir = 'd';
        if (('d' != previousdir) && (previousdir == 'i')) {
          nPk++;
          idx->data[nPk - 1] = kfirst;
        }
      } else {
        dir = 'i';
      }

      ykfirst = yk;
      kfirst = k;
      isinfykfirst = isinfyk;
    }
  }

  emxInit_int32_T(&iPk, 1);
  if (1 > nPk) {
    i = 0;
  } else {
    i = nPk;
  }

  i1 = idx->size[0];
  idx->size[0] = i;
  emxEnsureCapacity_int32_T(idx, i1);
  i1 = iInfinite->size[0];
  if (1 > nInf) {
    iInfinite->size[0] = 0;
  } else {
    iInfinite->size[0] = nInf;
  }

  emxEnsureCapacity_int32_T(iInfinite, i1);
  i1 = iPk->size[0];
  iPk->size[0] = i;
  emxEnsureCapacity_int32_T(iPk, i1);
  nPk = 0;
  for (k = 0; k < i; k++) {
    ykfirst = Yin->data[idx->data[k] - 1];
    if ((ykfirst > rtMinusInf) && (ykfirst - fmax(Yin->data[idx->data[k] - 2],
          Yin->data[idx->data[k]]) >= 0.0)) {
      nPk++;
      iPk->data[nPk - 1] = idx->data[k];
    }
  }

  emxInit_int32_T(&c, 1);
  i = iPk->size[0];
  if (1 > nPk) {
    iPk->size[0] = 0;
  } else {
    iPk->size[0] = nPk;
  }

  emxEnsureCapacity_int32_T(iPk, i);
  do_vectors(iPk, iInfinite, c, b_sortIdx, idx);
  emxFree_int32_T(&iInfinite);
  if (c->size[0] == 0) {
    idx->size[0] = 0;
  } else {
    n = c->size[0] + 1;
    i = b_sortIdx->size[0];
    b_sortIdx->size[0] = c->size[0];
    emxEnsureCapacity_int32_T(b_sortIdx, i);
    kfirst = c->size[0];
    for (i = 0; i < kfirst; i++) {
      b_sortIdx->data[i] = 0;
    }

    emxInit_int32_T(&iwork, 1);
    i = iwork->size[0];
    iwork->size[0] = c->size[0];
    emxEnsureCapacity_int32_T(iwork, i);
    i = c->size[0] - 1;
    for (k = 1; k <= i; k += 2) {
      i1 = c->data[k - 1] - 1;
      if ((Yin->data[i1] >= Yin->data[c->data[k] - 1]) || rtIsNaN(Yin->data[i1]))
      {
        b_sortIdx->data[k - 1] = k;
        b_sortIdx->data[k] = k + 1;
      } else {
        b_sortIdx->data[k - 1] = k + 1;
        b_sortIdx->data[k] = k;
      }
    }

    if ((c->size[0] & 1) != 0) {
      b_sortIdx->data[c->size[0] - 1] = c->size[0];
    }

    b_i = 2;
    while (b_i < n - 1) {
      ny = b_i << 1;
      kfirst = 1;
      for (nPk = b_i + 1; nPk < n; nPk = qEnd + b_i) {
        nInf = kfirst - 1;
        q = nPk;
        qEnd = kfirst + ny;
        if (qEnd > n) {
          qEnd = n;
        }

        k = 0;
        kEnd = qEnd - kfirst;
        while (k + 1 <= kEnd) {
          i = c->data[b_sortIdx->data[nInf] - 1] - 1;
          i1 = b_sortIdx->data[q - 1];
          if ((Yin->data[i] >= Yin->data[c->data[i1 - 1] - 1]) || rtIsNaN
              (Yin->data[i])) {
            iwork->data[k] = b_sortIdx->data[nInf];
            nInf++;
            if (nInf + 1 == nPk) {
              while (q < qEnd) {
                k++;
                iwork->data[k] = b_sortIdx->data[q - 1];
                q++;
              }
            }
          } else {
            iwork->data[k] = i1;
            q++;
            if (q == qEnd) {
              while (nInf + 1 < nPk) {
                k++;
                iwork->data[k] = b_sortIdx->data[nInf];
                nInf++;
              }
            }
          }

          k++;
        }

        for (k = 0; k < kEnd; k++) {
          b_sortIdx->data[(kfirst + k) - 1] = iwork->data[k];
        }

        kfirst = qEnd;
      }

      b_i = ny;
    }

    emxFree_int32_T(&iwork);
    emxInit_uint32_T(&locs_temp, 1);
    i = locs_temp->size[0];
    locs_temp->size[0] = b_sortIdx->size[0];
    emxEnsureCapacity_uint32_T(locs_temp, i);
    kfirst = b_sortIdx->size[0];
    for (i = 0; i < kfirst; i++) {
      locs_temp->data[i] = (unsigned int)c->data[b_sortIdx->data[i] - 1];
    }

    emxInit_boolean_T(&idelete, 1);
    i = idelete->size[0];
    idelete->size[0] = b_sortIdx->size[0];
    emxEnsureCapacity_boolean_T(idelete, i);
    kfirst = b_sortIdx->size[0];
    for (i = 0; i < kfirst; i++) {
      idelete->data[i] = false;
    }

    i = b_sortIdx->size[0];
    emxInit_boolean_T(&r, 1);
    for (b_i = 0; b_i < i; b_i++) {
      if (!idelete->data[b_i]) {
        i1 = r->size[0];
        r->size[0] = locs_temp->size[0];
        emxEnsureCapacity_boolean_T(r, i1);
        kfirst = locs_temp->size[0];
        for (i1 = 0; i1 < kfirst; i1++) {
          ny = c->data[b_sortIdx->data[b_i] - 1] - 1;
          r->data[i1] = (((int)locs_temp->data[i1] >= (int)x->data[ny] - 16) &&
                         (locs_temp->data[i1] <= x->data[ny] + 16U));
        }

        kfirst = idelete->size[0];
        for (i1 = 0; i1 < kfirst; i1++) {
          idelete->data[i1] = (idelete->data[i1] || r->data[i1]);
        }

        idelete->data[b_i] = false;
      }
    }

    emxFree_boolean_T(&r);
    emxFree_uint32_T(&locs_temp);
    kfirst = idelete->size[0] - 1;
    ny = 0;
    for (b_i = 0; b_i <= kfirst; b_i++) {
      if (!idelete->data[b_i]) {
        ny++;
      }
    }

    emxInit_int32_T(&r1, 1);
    i = r1->size[0];
    r1->size[0] = ny;
    emxEnsureCapacity_int32_T(r1, i);
    ny = 0;
    for (b_i = 0; b_i <= kfirst; b_i++) {
      if (!idelete->data[b_i]) {
        r1->data[ny] = b_i + 1;
        ny++;
      }
    }

    emxFree_boolean_T(&idelete);
    i = idx->size[0];
    idx->size[0] = r1->size[0];
    emxEnsureCapacity_int32_T(idx, i);
    kfirst = r1->size[0];
    for (i = 0; i < kfirst; i++) {
      idx->data[i] = b_sortIdx->data[r1->data[i] - 1];
    }

    emxFree_int32_T(&r1);
    sort(idx);
  }

  emxFree_int32_T(&b_sortIdx);
  emxFree_uint32_T(&x);
  if (idx->size[0] > Yin->size[0]) {
    i = idx->size[0];
    idx->size[0] = Yin->size[0];
    emxEnsureCapacity_int32_T(idx, i);
  }

  i = iPk->size[0];
  iPk->size[0] = idx->size[0];
  emxEnsureCapacity_int32_T(iPk, i);
  kfirst = idx->size[0];
  for (i = 0; i < kfirst; i++) {
    iPk->data[i] = c->data[idx->data[i] - 1];
  }

  emxFree_int32_T(&c);
  emxFree_int32_T(&idx);
  i = Ypk->size[0];
  Ypk->size[0] = iPk->size[0];
  emxEnsureCapacity_real_T(Ypk, i);
  kfirst = iPk->size[0];
  for (i = 0; i < kfirst; i++) {
    Ypk->data[i] = Yin->data[iPk->data[i] - 1];
  }

  i = Xpk->size[0];
  Xpk->size[0] = iPk->size[0];
  emxEnsureCapacity_real_T(Xpk, i);
  kfirst = iPk->size[0];
  for (i = 0; i < kfirst; i++) {
    Xpk->data[i] = (unsigned int)iPk->data[i];
  }

  emxFree_int32_T(&iPk);
}

/* End of code generation (findpeaks.c) */
