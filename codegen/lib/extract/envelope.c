/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * envelope.c
 *
 * Code generation for function 'envelope'
 *
 */

/* Include files */
#include "envelope.h"
#include "extract.h"
#include "extract_emxutil.h"
#include "findpeaks.h"
#include "interp1.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void envelope(const emxArray_real_T *x, emxArray_real_T *upperEnv,
              emxArray_real_T *lowerEnv)
{
  emxArray_real_T *b_x;
  int yk;
  int k;
  emxArray_real_T *r;
  emxArray_real_T *X;
  emxArray_real_T *V;
  emxArray_real_T *iPk;
  emxArray_int32_T *y;
  emxArray_real_T *b_y;
  int n;
  emxInit_real_T(&b_x, 1);
  yk = x->size[1];
  k = b_x->size[0];
  b_x->size[0] = x->size[1];
  emxEnsureCapacity_real_T(b_x, k);
  for (k = 0; k < yk; k++) {
    b_x->data[k] = x->data[k];
  }

  emxInit_real_T(&r, 1);
  emxInit_real_T(&X, 1);
  emxInit_real_T(&V, 1);
  emxInit_real_T(&iPk, 1);
  emxInit_int32_T(&y, 2);
  emxInit_real_T(&b_y, 1);
  if (b_x->size[0] < 2) {
    k = r->size[0];
    r->size[0] = b_x->size[0];
    emxEnsureCapacity_real_T(r, k);
    yk = b_x->size[0];
    for (k = 0; k < yk; k++) {
      r->data[k] = b_x->data[k];
    }
  } else {
    if (b_x->size[0] > 17) {
      findpeaks(b_x, X, iPk);
      if (iPk->size[0] == 0) {
        k = X->size[0];
        X->size[0] = 2;
        emxEnsureCapacity_real_T(X, k);
        X->data[0] = 1.0;
        X->data[1] = b_x->size[0];
        k = V->size[0];
        V->size[0] = 2;
        emxEnsureCapacity_real_T(V, k);
        V->data[0] = b_x->data[0];
        V->data[1] = b_x->data[b_x->size[0] - 1];
      } else if (iPk->size[0] == 1) {
        k = X->size[0];
        X->size[0] = 3;
        emxEnsureCapacity_real_T(X, k);
        X->data[0] = 1.0;
        X->data[1] = iPk->data[0];
        X->data[2] = b_x->size[0];
        k = V->size[0];
        V->size[0] = 3;
        emxEnsureCapacity_real_T(V, k);
        V->data[0] = b_x->data[0];
        V->data[1] = b_x->data[(int)iPk->data[0] - 1];
        V->data[2] = b_x->data[b_x->size[0] - 1];
      } else {
        k = X->size[0];
        X->size[0] = iPk->size[0];
        emxEnsureCapacity_real_T(X, k);
        yk = iPk->size[0];
        for (k = 0; k < yk; k++) {
          X->data[k] = iPk->data[k];
        }

        k = V->size[0];
        V->size[0] = iPk->size[0];
        emxEnsureCapacity_real_T(V, k);
        yk = iPk->size[0];
        for (k = 0; k < yk; k++) {
          V->data[k] = b_x->data[(int)iPk->data[k] - 1];
        }
      }
    } else {
      k = X->size[0];
      X->size[0] = 2;
      emxEnsureCapacity_real_T(X, k);
      X->data[0] = 1.0;
      X->data[1] = b_x->size[0];
      k = V->size[0];
      V->size[0] = 2;
      emxEnsureCapacity_real_T(V, k);
      V->data[0] = b_x->data[0];
      V->data[1] = b_x->data[b_x->size[0] - 1];
    }

    n = b_x->size[0];
    k = y->size[0] * y->size[1];
    y->size[0] = 1;
    y->size[1] = b_x->size[0];
    emxEnsureCapacity_int32_T(y, k);
    y->data[0] = 1;
    yk = 1;
    for (k = 2; k <= n; k++) {
      yk++;
      y->data[k - 1] = yk;
    }

    k = b_y->size[0];
    b_y->size[0] = y->size[1];
    emxEnsureCapacity_real_T(b_y, k);
    yk = y->size[1];
    for (k = 0; k < yk; k++) {
      b_y->data[k] = y->data[k];
    }

    interp1(X, V, b_y, r);
  }

  if (b_x->size[0] >= 2) {
    if (b_x->size[0] > 17) {
      k = b_y->size[0];
      b_y->size[0] = b_x->size[0];
      emxEnsureCapacity_real_T(b_y, k);
      yk = b_x->size[0];
      for (k = 0; k < yk; k++) {
        b_y->data[k] = -b_x->data[k];
      }

      findpeaks(b_y, X, iPk);
      if (iPk->size[0] == 0) {
        k = X->size[0];
        X->size[0] = 2;
        emxEnsureCapacity_real_T(X, k);
        X->data[0] = 1.0;
        X->data[1] = b_x->size[0];
        k = V->size[0];
        V->size[0] = 2;
        emxEnsureCapacity_real_T(V, k);
        V->data[0] = b_x->data[0];
        V->data[1] = b_x->data[b_x->size[0] - 1];
      } else if (iPk->size[0] == 1) {
        k = X->size[0];
        X->size[0] = 3;
        emxEnsureCapacity_real_T(X, k);
        X->data[0] = 1.0;
        X->data[1] = iPk->data[0];
        X->data[2] = b_x->size[0];
        k = V->size[0];
        V->size[0] = 3;
        emxEnsureCapacity_real_T(V, k);
        V->data[0] = b_x->data[0];
        V->data[1] = b_x->data[(int)iPk->data[0] - 1];
        V->data[2] = b_x->data[b_x->size[0] - 1];
      } else {
        k = X->size[0];
        X->size[0] = iPk->size[0];
        emxEnsureCapacity_real_T(X, k);
        yk = iPk->size[0];
        for (k = 0; k < yk; k++) {
          X->data[k] = iPk->data[k];
        }

        k = V->size[0];
        V->size[0] = iPk->size[0];
        emxEnsureCapacity_real_T(V, k);
        yk = iPk->size[0];
        for (k = 0; k < yk; k++) {
          V->data[k] = b_x->data[(int)iPk->data[k] - 1];
        }
      }
    } else {
      k = X->size[0];
      X->size[0] = 2;
      emxEnsureCapacity_real_T(X, k);
      X->data[0] = 1.0;
      X->data[1] = b_x->size[0];
      k = V->size[0];
      V->size[0] = 2;
      emxEnsureCapacity_real_T(V, k);
      V->data[0] = b_x->data[0];
      V->data[1] = b_x->data[b_x->size[0] - 1];
    }

    n = b_x->size[0];
    k = y->size[0] * y->size[1];
    y->size[0] = 1;
    y->size[1] = b_x->size[0];
    emxEnsureCapacity_int32_T(y, k);
    y->data[0] = 1;
    yk = 1;
    for (k = 2; k <= n; k++) {
      yk++;
      y->data[k - 1] = yk;
    }

    k = b_y->size[0];
    b_y->size[0] = y->size[1];
    emxEnsureCapacity_real_T(b_y, k);
    yk = y->size[1];
    for (k = 0; k < yk; k++) {
      b_y->data[k] = y->data[k];
    }

    interp1(X, V, b_y, b_x);
  }

  emxFree_real_T(&b_y);
  emxFree_int32_T(&y);
  emxFree_real_T(&iPk);
  emxFree_real_T(&V);
  emxFree_real_T(&X);
  k = upperEnv->size[0] * upperEnv->size[1];
  upperEnv->size[0] = 1;
  upperEnv->size[1] = x->size[1];
  emxEnsureCapacity_real_T(upperEnv, k);
  yk = x->size[1];
  for (k = 0; k < yk; k++) {
    upperEnv->data[k] = r->data[k];
  }

  emxFree_real_T(&r);
  k = lowerEnv->size[0] * lowerEnv->size[1];
  lowerEnv->size[0] = 1;
  lowerEnv->size[1] = x->size[1];
  emxEnsureCapacity_real_T(lowerEnv, k);
  yk = x->size[1];
  for (k = 0; k < yk; k++) {
    lowerEnv->data[k] = b_x->data[k];
  }

  emxFree_real_T(&b_x);
}

/* End of code generation (envelope.c) */
