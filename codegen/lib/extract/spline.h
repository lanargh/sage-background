/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * spline.h
 *
 * Code generation for function 'spline'
 *
 */

#ifndef SPLINE_H
#define SPLINE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern void spline(const emxArray_real_T *x, const emxArray_real_T *y,
                   emxArray_real_T *output_breaks, emxArray_real_T *output_coefs);

#endif

/* End of code generation (spline.h) */
