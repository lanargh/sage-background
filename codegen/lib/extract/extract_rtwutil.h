/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * extract_rtwutil.h
 *
 * Code generation for function 'extract_rtwutil'
 *
 */

#ifndef EXTRACT_RTWUTIL_H
#define EXTRACT_RTWUTIL_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern double rt_roundd_snf(double u);

#endif

/* End of code generation (extract_rtwutil.h) */
