/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * interp1.c
 *
 * Code generation for function 'interp1'
 *
 */

/* Include files */
#include "interp1.h"
#include "bsearch.h"
#include "extract.h"
#include "extract_emxutil.h"
#include "pchip.h"
#include "rt_nonfinite.h"
#include "spline.h"

/* Function Declarations */
static void interp1SplineOrPCHIP(const emxArray_real_T *y, const emxArray_real_T
  *xi, emxArray_real_T *yi, const emxArray_real_T *x);

/* Function Definitions */
static void interp1SplineOrPCHIP(const emxArray_real_T *y, const emxArray_real_T
  *xi, emxArray_real_T *yi, const emxArray_real_T *x)
{
  emxArray_real_T *b_y;
  int ub_loop;
  int loop_ub;
  struct_T pp;
  int k;
  int numTerms;
  int ip;
  double v;
  double xloc;
  int ic;
  emxInit_real_T(&b_y, 2);
  ub_loop = b_y->size[0] * b_y->size[1];
  b_y->size[0] = 1;
  b_y->size[1] = y->size[0];
  emxEnsureCapacity_real_T(b_y, ub_loop);
  loop_ub = y->size[0];
  for (ub_loop = 0; ub_loop < loop_ub; ub_loop++) {
    b_y->data[ub_loop] = y->data[ub_loop];
  }

  emxInitStruct_struct_T(&pp);
  spline(x, b_y, pp.breaks, pp.coefs);
  ub_loop = xi->size[0] - 1;
  emxFree_real_T(&b_y);

#pragma omp parallel for \
 num_threads(omp_get_max_threads()) \
 private(numTerms,ip,v,xloc,ic)

  for (k = 0; k <= ub_loop; k++) {
    if (rtIsNaN(xi->data[k])) {
      yi->data[k] = rtNaN;
    } else {
      numTerms = pp.coefs->size[1];
      if (rtIsNaN(xi->data[k])) {
        v = xi->data[k];
      } else {
        ip = b_bsearch(pp.breaks, xi->data[k]) - 1;
        xloc = xi->data[k] - pp.breaks->data[ip];
        v = pp.coefs->data[ip];
        for (ic = 2; ic <= numTerms; ic++) {
          v = xloc * v + pp.coefs->data[ip + (ic - 1) * (pp.breaks->size[1] - 1)];
        }
      }

      yi->data[k] = v;
    }
  }

  emxFreeStruct_struct_T(&pp);
}

void b_interp1SplineOrPCHIP(const emxArray_real_T *y, const emxArray_real_T *xi,
  emxArray_real_T *yi, const emxArray_real_T *x)
{
  emxArray_real_T *h;
  int nxm1;
  int k;
  emxArray_real_T *del;
  emxArray_real_T *slopes;
  int nxm2;
  double d;
  struct_T pp;
  double d1;
  double hs;
  double hs3;
  double dzzdx;
  int cpage;
  int b_k;
  double xloc;
  int ip;
  emxInit_real_T(&h, 2);
  nxm1 = x->size[1] - 2;
  k = h->size[0] * h->size[1];
  h->size[0] = 1;
  h->size[1] = x->size[1] - 1;
  emxEnsureCapacity_real_T(h, k);
  for (k = 0; k <= nxm1; k++) {
    h->data[k] = x->data[k + 1] - x->data[k];
  }

  emxInit_real_T(&del, 2);
  k = del->size[0] * del->size[1];
  del->size[0] = 1;
  del->size[1] = y->size[0] - 1;
  emxEnsureCapacity_real_T(del, k);
  for (k = 0; k <= nxm1; k++) {
    del->data[k] = (y->data[k + 1] - y->data[k]) / h->data[k];
  }

  emxInit_real_T(&slopes, 2);
  k = slopes->size[0] * slopes->size[1];
  slopes->size[0] = 1;
  slopes->size[1] = y->size[0];
  emxEnsureCapacity_real_T(slopes, k);
  if (x->size[1] == 2) {
    slopes->data[0] = del->data[0];
    slopes->data[1] = del->data[0];
  } else {
    nxm2 = x->size[1];
    for (k = 0; k <= nxm2 - 3; k++) {
      d = h->data[k];
      d1 = h->data[k + 1];
      hs = d + d1;
      hs3 = 3.0 * hs;
      dzzdx = (d + hs) / hs3;
      hs = (d1 + hs) / hs3;
      slopes->data[k + 1] = 0.0;
      d = del->data[k];
      if (d < 0.0) {
        d1 = del->data[k + 1];
        if (d1 <= d) {
          slopes->data[k + 1] = d / (dzzdx * (d / d1) + hs);
        } else {
          if (d1 < 0.0) {
            slopes->data[k + 1] = d1 / (dzzdx + hs * (d1 / d));
          }
        }
      } else {
        if (d > 0.0) {
          d1 = del->data[k + 1];
          if (d1 >= d) {
            slopes->data[k + 1] = d / (dzzdx * (d / del->data[k + 1]) + hs);
          } else {
            if (d1 > 0.0) {
              slopes->data[k + 1] = del->data[k + 1] / (dzzdx + hs * (del->
                data[k + 1] / d));
            }
          }
        }
      }
    }

    slopes->data[0] = exteriorSlope(del->data[0], del->data[1], h->data[0],
      h->data[1]);
    slopes->data[x->size[1] - 1] = exteriorSlope(del->data[x->size[1] - 2],
      del->data[x->size[1] - 3], h->data[x->size[1] - 2], h->data[x->size[1] - 3]);
  }

  emxInitStruct_struct_T(&pp);
  nxm1 = x->size[1];
  k = pp.breaks->size[0] * pp.breaks->size[1];
  pp.breaks->size[0] = 1;
  pp.breaks->size[1] = x->size[1];
  emxEnsureCapacity_real_T(pp.breaks, k);
  nxm2 = x->size[1];
  for (k = 0; k < nxm2; k++) {
    pp.breaks->data[k] = x->data[k];
  }

  cpage = slopes->size[1] - 1;
  k = pp.coefs->size[0] * pp.coefs->size[1];
  pp.coefs->size[0] = slopes->size[1] - 1;
  pp.coefs->size[1] = 4;
  emxEnsureCapacity_real_T(pp.coefs, k);
  for (nxm2 = 0; nxm2 <= nxm1 - 2; nxm2++) {
    d = del->data[nxm2];
    d1 = slopes->data[nxm2];
    hs3 = h->data[nxm2];
    dzzdx = (d - d1) / hs3;
    hs = (slopes->data[nxm2 + 1] - d) / hs3;
    pp.coefs->data[nxm2] = (hs - dzzdx) / hs3;
    pp.coefs->data[cpage + nxm2] = 2.0 * dzzdx - hs;
    pp.coefs->data[(cpage << 1) + nxm2] = d1;
    pp.coefs->data[3 * cpage + nxm2] = y->data[nxm2];
  }

  emxFree_real_T(&slopes);
  emxFree_real_T(&del);
  emxFree_real_T(&h);
  nxm2 = xi->size[1] - 1;

#pragma omp parallel for \
 num_threads(omp_get_max_threads()) \
 private(xloc,ip)

  for (b_k = 0; b_k <= nxm2; b_k++) {
    xloc = xi->data[b_k];
    if (rtIsNaN(xloc)) {
      yi->data[b_k] = rtNaN;
    } else {
      if (!rtIsNaN(xloc)) {
        ip = b_bsearch(pp.breaks, xloc) - 1;
        xloc -= pp.breaks->data[ip];
        xloc = xloc * (xloc * (xloc * pp.coefs->data[ip] + pp.coefs->data[(ip +
          pp.breaks->size[1]) - 1]) + pp.coefs->data[ip + 2 * (pp.breaks->size[1]
          - 1)]) + pp.coefs->data[ip + 3 * (pp.breaks->size[1] - 1)];
      }

      yi->data[b_k] = xloc;
    }
  }

  emxFreeStruct_struct_T(&pp);
}

void interp1(const emxArray_real_T *varargin_1, const emxArray_real_T
             *varargin_2, const emxArray_real_T *varargin_3, emxArray_real_T *Vq)
{
  emxArray_real_T *y;
  int y_tmp;
  int n;
  emxArray_real_T *x;
  int nx;
  int k;
  int exitg1;
  double xtmp;
  int nd2;
  emxInit_real_T(&y, 1);
  y_tmp = y->size[0];
  y->size[0] = varargin_2->size[0];
  emxEnsureCapacity_real_T(y, y_tmp);
  n = varargin_2->size[0];
  for (y_tmp = 0; y_tmp < n; y_tmp++) {
    y->data[y_tmp] = varargin_2->data[y_tmp];
  }

  emxInit_real_T(&x, 1);
  y_tmp = x->size[0];
  x->size[0] = varargin_1->size[0];
  emxEnsureCapacity_real_T(x, y_tmp);
  n = varargin_1->size[0];
  for (y_tmp = 0; y_tmp < n; y_tmp++) {
    x->data[y_tmp] = varargin_1->data[y_tmp];
  }

  nx = varargin_1->size[0] - 1;
  y_tmp = Vq->size[0];
  Vq->size[0] = varargin_3->size[0];
  emxEnsureCapacity_real_T(Vq, y_tmp);
  n = varargin_3->size[0];
  for (y_tmp = 0; y_tmp < n; y_tmp++) {
    Vq->data[y_tmp] = 0.0;
  }

  k = 0;
  do {
    exitg1 = 0;
    if (k <= nx) {
      if (rtIsNaN(varargin_1->data[k])) {
        exitg1 = 1;
      } else {
        k++;
      }
    } else {
      if (varargin_1->data[1] < varargin_1->data[0]) {
        y_tmp = (nx + 1) >> 1;
        for (n = 0; n < y_tmp; n++) {
          xtmp = x->data[n];
          nd2 = nx - n;
          x->data[n] = x->data[nd2];
          x->data[nd2] = xtmp;
        }

        if ((varargin_2->size[0] != 0) && (varargin_2->size[0] > 1)) {
          n = varargin_2->size[0] - 1;
          nd2 = varargin_2->size[0] >> 1;
          for (k = 0; k < nd2; k++) {
            xtmp = y->data[k];
            y_tmp = n - k;
            y->data[k] = y->data[y_tmp];
            y->data[y_tmp] = xtmp;
          }
        }
      }

      interp1SplineOrPCHIP(y, varargin_3, Vq, x);
      exitg1 = 1;
    }
  } while (exitg1 == 0);

  emxFree_real_T(&x);
  emxFree_real_T(&y);
}

/* End of code generation (interp1.c) */
