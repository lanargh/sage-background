/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * extract_terminate.h
 *
 * Code generation for function 'extract_terminate'
 *
 */

#ifndef EXTRACT_TERMINATE_H
#define EXTRACT_TERMINATE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "omp.h"
#include "extract_types.h"

/* Function Declarations */
extern void extract_terminate(void);

#endif

/* End of code generation (extract_terminate.h) */
