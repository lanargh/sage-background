/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * extract.c
 *
 * Code generation for function 'extract'
 *
 */

/* Include files */
#include "extract.h"
#include "ecgwindowedfilter.h"
#include "envelope.h"
#include "extract_data.h"
#include "extract_emxutil.h"
#include "extract_initialize.h"
#include "extract_rtwutil.h"
#include "fft.h"
#include "find.h"
#include "ifft.h"
#include "inflection_point.h"
#include "max_slope.h"
#include "ppgannotation.h"
#include "rt_nonfinite.h"
#include "trapz.h"
#include "unique.h"
#include <math.h>

/* Function Definitions */
void extract(const emxArray_real_T *ecg, const emxArray_real_T *ppg,
             emxArray_real_T *features)
{
  emxArray_int32_T *the_first_point;
  emxArray_int32_T *the_second_point;
  emxArray_real_T *f_hr_values;
  emxArray_real_T *f_ptt_values;
  emxArray_real_T *f_ai_values;
  emxArray_real_T *f_uptime_values;
  emxArray_real_T *f_syst_time_values;
  emxArray_real_T *f_dv_values;
  emxArray_real_T *f_sv_values;
  emxArray_creal_T *ecg_fftresult;
  emxArray_real_T *detected_ecg_peaks;
  emxArray_real_T *notch;
  int i;
  int loop_ub;
  int i1;
  emxArray_real_T *filtered_ecg;
  int i2;
  emxArray_creal_T *r;
  emxArray_real_T *b_detected_ecg_peaks;
  int n;
  int idx;
  double dist_error;
  int k;
  boolean_T exitg1;
  emxArray_boolean_T *c_detected_ecg_peaks;
  double d;
  emxArray_int32_T *locs_ecg_peak;
  emxArray_int32_T *r1;
  emxArray_real_T *filtered_ppg;
  emxArray_real_T *systolic_peak;
  emxArray_real_T *dicroticIndex;
  emxArray_real_T *foot;
  emxArray_real_T *b_dicroticIndex;
  int loccc;
  emxArray_real_T *PPG_min_positions;
  emxArray_real_T *PPG_min_positions_after;
  emxArray_real_T *dict_notch;
  emxArray_real_T *b_filtered_ecg;
  emxArray_boolean_T *r2;
  emxArray_boolean_T *r3;
  emxArray_boolean_T *r4;
  emxArray_boolean_T *r5;
  emxArray_boolean_T *r6;
  emxArray_boolean_T *r7;
  emxArray_real_T *b_f_uptime_values;
  int peak;
  int ecg_peak1;
  int ecg_peak2;
  double ppg_peak1;
  double ppg_peak2;
  double index_dict_notch;
  int the_first_point_idx_0;
  boolean_T empty_non_axis_sizes;
  signed char i3;
  int b_loop_ub;
  int b_the_first_point_idx_0;
  int c_the_first_point_idx_0;
  int d_the_first_point_idx_0;
  if (isInitialized_extract == false) {
    extract_initialize();
  }

  emxInit_int32_T(&the_first_point, 1);
  emxInit_int32_T(&the_second_point, 1);
  emxInit_real_T(&f_hr_values, 1);
  emxInit_real_T(&f_ptt_values, 1);
  emxInit_real_T(&f_ai_values, 1);
  emxInit_real_T(&f_uptime_values, 2);
  emxInit_real_T(&f_syst_time_values, 2);
  emxInit_real_T(&f_dv_values, 1);
  emxInit_real_T(&f_sv_values, 1);
  emxInit_creal_T(&ecg_fftresult, 2);
  emxInit_real_T(&detected_ecg_peaks, 2);
  emxInit_real_T(&notch, 2);

  /*     %% Initialization */
  /*  Features */
  i = the_first_point->size[0];
  the_first_point->size[0] = 1;
  emxEnsureCapacity_int32_T(the_first_point, i);
  the_first_point->data[0] = 0;
  i = the_second_point->size[0];
  the_second_point->size[0] = 1;
  emxEnsureCapacity_int32_T(the_second_point, i);
  the_second_point->data[0] = 0;
  i = f_hr_values->size[0];
  f_hr_values->size[0] = 1;
  emxEnsureCapacity_real_T(f_hr_values, i);
  f_hr_values->data[0] = 0.0;
  i = f_ptt_values->size[0];
  f_ptt_values->size[0] = 1;
  emxEnsureCapacity_real_T(f_ptt_values, i);
  f_ptt_values->data[0] = 0.0;
  i = f_ai_values->size[0];
  f_ai_values->size[0] = 1;
  emxEnsureCapacity_real_T(f_ai_values, i);
  f_ai_values->data[0] = 0.0;
  f_uptime_values->size[0] = 0;
  f_uptime_values->size[1] = 0;
  f_syst_time_values->size[0] = 0;
  f_syst_time_values->size[1] = 0;
  i = f_dv_values->size[0];
  f_dv_values->size[0] = 1;
  emxEnsureCapacity_real_T(f_dv_values, i);
  f_dv_values->data[0] = 0.0;
  i = f_sv_values->size[0];
  f_sv_values->size[0] = 1;
  emxEnsureCapacity_real_T(f_sv_values, i);
  f_sv_values->data[0] = 0.0;
  i = features->size[0] * features->size[1];
  features->size[0] = 1;
  features->size[1] = 1;
  emxEnsureCapacity_real_T(features, i);
  features->data[0] = 0.0;

  /*     %% Preprocessing */
  /*    Smoothing */
  envelope(ppg, detected_ecg_peaks, notch);

  /*    Transform ecg & ppg into frequency spectrum - Remove the low */
  /*    frequency components - Inverse to transform back to time domain */
  fft(ecg, ecg_fftresult);
  i = (int)rt_roundd_snf((double)ecg_fftresult->size[1] / 256.0);
  if (1 > i) {
    loop_ub = 0;
  } else {
    loop_ub = i;
  }

  for (i = 0; i < loop_ub; i++) {
    ecg_fftresult->data[i].re = 0.0;
    ecg_fftresult->data[i].im = 0.0;
  }

  i = ecg_fftresult->size[1] - (int)rt_roundd_snf((double)ecg_fftresult->size[1]
    / 256.0);
  if (i > ecg_fftresult->size[1]) {
    i = -1;
    i1 = 0;
  } else {
    i -= 2;
    i1 = ecg_fftresult->size[1];
  }

  loop_ub = (i1 - i) - 1;
  for (i1 = 0; i1 < loop_ub; i1++) {
    i2 = (i + i1) + 1;
    ecg_fftresult->data[i2].re = 0.0;
    ecg_fftresult->data[i2].im = 0.0;
  }

  emxInit_real_T(&filtered_ecg, 2);
  emxInit_creal_T(&r, 2);
  ifft(ecg_fftresult, r);
  i = filtered_ecg->size[0] * filtered_ecg->size[1];
  filtered_ecg->size[0] = 1;
  filtered_ecg->size[1] = r->size[1];
  emxEnsureCapacity_real_T(filtered_ecg, i);
  loop_ub = r->size[0] * r->size[1];
  for (i = 0; i < loop_ub; i++) {
    filtered_ecg->data[i] = r->data[i].re;
  }

  emxInit_real_T(&b_detected_ecg_peaks, 2);
  i = b_detected_ecg_peaks->size[0] * b_detected_ecg_peaks->size[1];
  b_detected_ecg_peaks->size[0] = 1;
  b_detected_ecg_peaks->size[1] = detected_ecg_peaks->size[1];
  emxEnsureCapacity_real_T(b_detected_ecg_peaks, i);
  loop_ub = detected_ecg_peaks->size[0] * detected_ecg_peaks->size[1];
  for (i = 0; i < loop_ub; i++) {
    b_detected_ecg_peaks->data[i] = (detected_ecg_peaks->data[i] + notch->data[i])
      / 2.0;
  }

  fft(b_detected_ecg_peaks, ecg_fftresult);
  i = (int)rt_roundd_snf((double)ecg_fftresult->size[1] / 256.0);
  if (1 > i) {
    loop_ub = 0;
  } else {
    loop_ub = i;
  }

  for (i = 0; i < loop_ub; i++) {
    ecg_fftresult->data[i].re = 0.0;
    ecg_fftresult->data[i].im = 0.0;
  }

  i = ecg_fftresult->size[1] - (int)rt_roundd_snf((double)ecg_fftresult->size[1]
    / 256.0);
  if (i > ecg_fftresult->size[1]) {
    i = -1;
    i1 = 0;
  } else {
    i -= 2;
    i1 = ecg_fftresult->size[1];
  }

  loop_ub = (i1 - i) - 1;
  for (i1 = 0; i1 < loop_ub; i1++) {
    i2 = (i + i1) + 1;
    ecg_fftresult->data[i2].re = 0.0;
    ecg_fftresult->data[i2].im = 0.0;
  }

  /*    Find peaks in ECG */
  ecgwindowedfilter(filtered_ecg, detected_ecg_peaks);

  /*    Scale ecg */
  n = detected_ecg_peaks->size[1];
  if (detected_ecg_peaks->size[1] <= 2) {
    if (detected_ecg_peaks->size[1] == 1) {
      dist_error = detected_ecg_peaks->data[0];
    } else if ((detected_ecg_peaks->data[0] < detected_ecg_peaks->data[1]) ||
               (rtIsNaN(detected_ecg_peaks->data[0]) && (!rtIsNaN
                 (detected_ecg_peaks->data[1])))) {
      dist_error = detected_ecg_peaks->data[1];
    } else {
      dist_error = detected_ecg_peaks->data[0];
    }
  } else {
    if (!rtIsNaN(detected_ecg_peaks->data[0])) {
      idx = 1;
    } else {
      idx = 0;
      k = 2;
      exitg1 = false;
      while ((!exitg1) && (k <= detected_ecg_peaks->size[1])) {
        if (!rtIsNaN(detected_ecg_peaks->data[k - 1])) {
          idx = k;
          exitg1 = true;
        } else {
          k++;
        }
      }
    }

    if (idx == 0) {
      dist_error = detected_ecg_peaks->data[0];
    } else {
      dist_error = detected_ecg_peaks->data[idx - 1];
      i = idx + 1;
      for (k = i; k <= n; k++) {
        d = detected_ecg_peaks->data[k - 1];
        if (dist_error < d) {
          dist_error = d;
        }
      }
    }
  }

  emxInit_boolean_T(&c_detected_ecg_peaks, 2);
  dist_error /= 7.0;
  i = c_detected_ecg_peaks->size[0] * c_detected_ecg_peaks->size[1];
  c_detected_ecg_peaks->size[0] = 1;
  c_detected_ecg_peaks->size[1] = detected_ecg_peaks->size[1];
  emxEnsureCapacity_boolean_T(c_detected_ecg_peaks, i);
  loop_ub = detected_ecg_peaks->size[0] * detected_ecg_peaks->size[1];
  for (i = 0; i < loop_ub; i++) {
    c_detected_ecg_peaks->data[i] = (detected_ecg_peaks->data[i] / dist_error >
      0.0);
  }

  emxInit_int32_T(&locs_ecg_peak, 2);
  emxInit_int32_T(&r1, 2);
  eml_find(c_detected_ecg_peaks, r1);
  i = locs_ecg_peak->size[0] * locs_ecg_peak->size[1];
  locs_ecg_peak->size[0] = 1;
  locs_ecg_peak->size[1] = r1->size[1];
  emxEnsureCapacity_int32_T(locs_ecg_peak, i);
  loop_ub = r1->size[0] * r1->size[1];
  for (i = 0; i < loop_ub; i++) {
    locs_ecg_peak->data[i] = r1->data[i];
  }

  emxInit_real_T(&filtered_ppg, 1);

  /*    Returns minimum distance between two peaks */
  ifft(ecg_fftresult, r);
  i = filtered_ppg->size[0];
  filtered_ppg->size[0] = r->size[1];
  emxEnsureCapacity_real_T(filtered_ppg, i);
  loop_ub = r->size[1];
  emxFree_creal_T(&ecg_fftresult);
  for (i = 0; i < loop_ub; i++) {
    filtered_ppg->data[i] = r->data[i].re;
  }

  emxFree_creal_T(&r);

  /*    Normalization (0,1) */
  n = filtered_ppg->size[0];
  if (filtered_ppg->size[0] <= 2) {
    if (filtered_ppg->size[0] == 1) {
      dist_error = filtered_ppg->data[0];
    } else if ((filtered_ppg->data[0] > filtered_ppg->data[1]) || (rtIsNaN
                (filtered_ppg->data[0]) && (!rtIsNaN(filtered_ppg->data[1])))) {
      dist_error = filtered_ppg->data[1];
    } else {
      dist_error = filtered_ppg->data[0];
    }
  } else {
    if (!rtIsNaN(filtered_ppg->data[0])) {
      idx = 1;
    } else {
      idx = 0;
      k = 2;
      exitg1 = false;
      while ((!exitg1) && (k <= filtered_ppg->size[0])) {
        if (!rtIsNaN(filtered_ppg->data[k - 1])) {
          idx = k;
          exitg1 = true;
        } else {
          k++;
        }
      }
    }

    if (idx == 0) {
      dist_error = filtered_ppg->data[0];
    } else {
      dist_error = filtered_ppg->data[idx - 1];
      i = idx + 1;
      for (k = i; k <= n; k++) {
        d = filtered_ppg->data[k - 1];
        if (dist_error > d) {
          dist_error = d;
        }
      }
    }
  }

  loop_ub = filtered_ppg->size[0];
  for (i = 0; i < loop_ub; i++) {
    filtered_ppg->data[i] -= dist_error;
  }

  n = filtered_ppg->size[0];
  if (filtered_ppg->size[0] <= 2) {
    if (filtered_ppg->size[0] == 1) {
      dist_error = filtered_ppg->data[0];
    } else if ((filtered_ppg->data[0] < filtered_ppg->data[1]) || (rtIsNaN
                (filtered_ppg->data[0]) && (!rtIsNaN(filtered_ppg->data[1])))) {
      dist_error = filtered_ppg->data[1];
    } else {
      dist_error = filtered_ppg->data[0];
    }
  } else {
    if (!rtIsNaN(filtered_ppg->data[0])) {
      idx = 1;
    } else {
      idx = 0;
      k = 2;
      exitg1 = false;
      while ((!exitg1) && (k <= filtered_ppg->size[0])) {
        if (!rtIsNaN(filtered_ppg->data[k - 1])) {
          idx = k;
          exitg1 = true;
        } else {
          k++;
        }
      }
    }

    if (idx == 0) {
      dist_error = filtered_ppg->data[0];
    } else {
      dist_error = filtered_ppg->data[idx - 1];
      i = idx + 1;
      for (k = i; k <= n; k++) {
        d = filtered_ppg->data[k - 1];
        if (dist_error < d) {
          dist_error = d;
        }
      }
    }
  }

  loop_ub = filtered_ppg->size[0];
  for (i = 0; i < loop_ub; i++) {
    filtered_ppg->data[i] /= dist_error;
  }

  emxInit_real_T(&systolic_peak, 1);
  emxInit_real_T(&dicroticIndex, 2);

  /*    Find peaks in PPG */
  ppgannotation(filtered_ppg, filtered_ecg, detected_ecg_peaks,
                b_detected_ecg_peaks, dicroticIndex);
  unique_vector(detected_ecg_peaks, notch);
  i = systolic_peak->size[0];
  systolic_peak->size[0] = notch->size[1];
  emxEnsureCapacity_real_T(systolic_peak, i);
  loop_ub = notch->size[1];
  for (i = 0; i < loop_ub; i++) {
    systolic_peak->data[i] = notch->data[i];
  }

  emxInit_real_T(&foot, 2);
  emxInit_real_T(&b_dicroticIndex, 2);
  unique_vector(filtered_ecg, foot);
  unique_vector(b_detected_ecg_peaks, notch);
  unique_vector(dicroticIndex, b_dicroticIndex);
  if (locs_ecg_peak->size[1] > systolic_peak->size[0]) {
    loccc = systolic_peak->size[0];
  } else {
    loccc = locs_ecg_peak->size[1];
  }

  /*     %% Feature Extraction */
  emxFree_real_T(&b_dicroticIndex);
  emxFree_real_T(&b_detected_ecg_peaks);
  emxFree_real_T(&dicroticIndex);
  emxFree_real_T(&filtered_ecg);
  emxInit_real_T(&PPG_min_positions, 2);
  emxInit_real_T(&PPG_min_positions_after, 2);
  emxInit_real_T(&dict_notch, 2);
  emxInit_real_T(&b_filtered_ecg, 1);
  emxInit_boolean_T(&r2, 2);
  emxInit_boolean_T(&r3, 2);
  emxInit_boolean_T(&r4, 2);
  emxInit_boolean_T(&r5, 2);
  emxInit_boolean_T(&r6, 2);
  emxInit_boolean_T(&r7, 2);
  emxInit_real_T(&b_f_uptime_values, 2);
  for (peak = 0; peak <= loccc - 3; peak++) {
    if (peak + 1 != loccc) {
      ecg_peak1 = locs_ecg_peak->data[peak];
      ecg_peak2 = locs_ecg_peak->data[peak + 1];
      ppg_peak1 = systolic_peak->data[peak];
      ppg_peak2 = systolic_peak->data[peak + 1];
      dist_error = fabs(ppg_peak2 - (double)ecg_peak1);
      if (dist_error > 512.0) {
        ecg_peak1 = ecg_peak2;
        ecg_peak2 = locs_ecg_peak->data[peak + 2];
      } else {
        if ((dist_error < 128.0) && (dist_error > 0.0)) {
          ppg_peak1 = ppg_peak2;
          ppg_peak2 = systolic_peak->data[peak + 2];
        }
      }

      i = r2->size[0] * r2->size[1];
      r2->size[0] = 1;
      r2->size[1] = foot->size[1];
      emxEnsureCapacity_boolean_T(r2, i);
      loop_ub = foot->size[0] * foot->size[1];
      i = r3->size[0] * r3->size[1];
      r3->size[0] = 1;
      r3->size[1] = foot->size[1];
      emxEnsureCapacity_boolean_T(r3, i);
      for (i = 0; i < loop_ub; i++) {
        d = foot->data[i];
        r2->data[i] = (d >= ecg_peak1);
        r3->data[i] = (d <= ecg_peak2);
      }

      i = r4->size[0] * r4->size[1];
      r4->size[0] = 1;
      r4->size[1] = notch->size[1];
      emxEnsureCapacity_boolean_T(r4, i);
      loop_ub = notch->size[0] * notch->size[1];
      i = r5->size[0] * r5->size[1];
      r5->size[0] = 1;
      r5->size[1] = notch->size[1];
      emxEnsureCapacity_boolean_T(r5, i);
      for (i = 0; i < loop_ub; i++) {
        d = notch->data[i];
        r4->data[i] = (d >= ecg_peak1);
        r5->data[i] = (d <= ppg_peak2);
      }

      i = r6->size[0] * r6->size[1];
      r6->size[0] = 1;
      r6->size[1] = foot->size[1];
      emxEnsureCapacity_boolean_T(r6, i);
      loop_ub = foot->size[0] * foot->size[1];
      i = r7->size[0] * r7->size[1];
      r7->size[0] = 1;
      r7->size[1] = foot->size[1];
      emxEnsureCapacity_boolean_T(r7, i);
      for (i = 0; i < loop_ub; i++) {
        d = foot->data[i];
        r6->data[i] = (d >= ecg_peak1);
        r7->data[i] = (d <= ppg_peak2);
      }

      k = r2->size[1] - 1;
      idx = 0;
      for (n = 0; n <= k; n++) {
        if (r2->data[n] && r3->data[n]) {
          idx++;
        }
      }

      i = PPG_min_positions->size[0] * PPG_min_positions->size[1];
      PPG_min_positions->size[0] = 1;
      PPG_min_positions->size[1] = idx;
      emxEnsureCapacity_real_T(PPG_min_positions, i);
      idx = 0;
      for (n = 0; n <= k; n++) {
        if (r2->data[n] && r3->data[n]) {
          PPG_min_positions->data[idx] = foot->data[n] + 1.0;
          idx++;
        }
      }

      k = r6->size[1] - 1;
      idx = 0;
      for (n = 0; n <= k; n++) {
        if (r6->data[n] && r7->data[n]) {
          idx++;
        }
      }

      i = PPG_min_positions_after->size[0] * PPG_min_positions_after->size[1];
      PPG_min_positions_after->size[0] = 1;
      PPG_min_positions_after->size[1] = idx;
      emxEnsureCapacity_real_T(PPG_min_positions_after, i);
      idx = 0;
      for (n = 0; n <= k; n++) {
        if (r6->data[n] && r7->data[n]) {
          PPG_min_positions_after->data[idx] = foot->data[n] + 1.0;
          idx++;
        }
      }

      k = r4->size[1] - 1;
      idx = 0;
      for (n = 0; n <= k; n++) {
        if (r4->data[n] && r5->data[n]) {
          idx++;
        }
      }

      i = dict_notch->size[0] * dict_notch->size[1];
      dict_notch->size[0] = 1;
      dict_notch->size[1] = idx;
      emxEnsureCapacity_real_T(dict_notch, i);
      idx = 0;
      for (n = 0; n <= k; n++) {
        if (r4->data[n] && r5->data[n]) {
          dict_notch->data[idx] = notch->data[n];
          idx++;
        }
      }

      dist_error = 0.0;
      ppg_peak2 = 0.0;
      index_dict_notch = 0.0;
      if (PPG_min_positions_after->size[1] > 1) {
        n = PPG_min_positions_after->size[1];
        if (PPG_min_positions_after->size[1] <= 2) {
          if ((PPG_min_positions_after->data[0] < PPG_min_positions_after->data
               [1]) || (rtIsNaN(PPG_min_positions_after->data[0]) && (!rtIsNaN
                (PPG_min_positions_after->data[1])))) {
            ppg_peak2 = PPG_min_positions_after->data[1];
          } else {
            ppg_peak2 = PPG_min_positions_after->data[0];
          }
        } else {
          if (!rtIsNaN(PPG_min_positions_after->data[0])) {
            idx = 1;
          } else {
            idx = 0;
            k = 2;
            exitg1 = false;
            while ((!exitg1) && (k <= PPG_min_positions_after->size[1])) {
              if (!rtIsNaN(PPG_min_positions_after->data[k - 1])) {
                idx = k;
                exitg1 = true;
              } else {
                k++;
              }
            }
          }

          if (idx == 0) {
            ppg_peak2 = PPG_min_positions_after->data[0];
          } else {
            ppg_peak2 = PPG_min_positions_after->data[idx - 1];
            i = idx + 1;
            for (k = i; k <= n; k++) {
              d = PPG_min_positions_after->data[k - 1];
              if (ppg_peak2 < d) {
                ppg_peak2 = d;
              }
            }
          }
        }
      } else {
        if (PPG_min_positions_after->size[1] == 1) {
          ppg_peak2 = PPG_min_positions_after->data[0];
        }
      }

      if (PPG_min_positions->size[1] > 1) {
        n = PPG_min_positions->size[1];
        if (PPG_min_positions->size[1] <= 2) {
          if ((PPG_min_positions->data[0] < PPG_min_positions->data[1]) ||
              (rtIsNaN(PPG_min_positions->data[0]) && (!rtIsNaN
                (PPG_min_positions->data[1])))) {
            dist_error = PPG_min_positions->data[1];
          } else {
            dist_error = PPG_min_positions->data[0];
          }
        } else {
          if (!rtIsNaN(PPG_min_positions->data[0])) {
            idx = 1;
          } else {
            idx = 0;
            k = 2;
            exitg1 = false;
            while ((!exitg1) && (k <= PPG_min_positions->size[1])) {
              if (!rtIsNaN(PPG_min_positions->data[k - 1])) {
                idx = k;
                exitg1 = true;
              } else {
                k++;
              }
            }
          }

          if (idx == 0) {
            dist_error = PPG_min_positions->data[0];
          } else {
            dist_error = PPG_min_positions->data[idx - 1];
            i = idx + 1;
            for (k = i; k <= n; k++) {
              d = PPG_min_positions->data[k - 1];
              if (dist_error < d) {
                dist_error = d;
              }
            }
          }
        }
      } else {
        if (PPG_min_positions->size[1] == 1) {
          dist_error = PPG_min_positions->data[0];
        }
      }

      if (dict_notch->size[1] > 1) {
        n = dict_notch->size[1];
        if (dict_notch->size[1] <= 2) {
          if ((dict_notch->data[0] < dict_notch->data[1]) || (rtIsNaN
               (dict_notch->data[0]) && (!rtIsNaN(dict_notch->data[1])))) {
            index_dict_notch = dict_notch->data[1];
          } else {
            index_dict_notch = dict_notch->data[0];
          }
        } else {
          if (!rtIsNaN(dict_notch->data[0])) {
            idx = 1;
          } else {
            idx = 0;
            k = 2;
            exitg1 = false;
            while ((!exitg1) && (k <= dict_notch->size[1])) {
              if (!rtIsNaN(dict_notch->data[k - 1])) {
                idx = k;
                exitg1 = true;
              } else {
                k++;
              }
            }
          }

          if (idx == 0) {
            index_dict_notch = dict_notch->data[0];
          } else {
            index_dict_notch = dict_notch->data[idx - 1];
            i = idx + 1;
            for (k = i; k <= n; k++) {
              d = dict_notch->data[k - 1];
              if (index_dict_notch < d) {
                index_dict_notch = d;
              }
            }
          }
        }
      } else {
        if (dict_notch->size[1] == 1) {
          index_dict_notch = dict_notch->data[0];
        }
      }

      if (dist_error > index_dict_notch) {
        i = 0;
        i1 = 0;
      } else {
        i = (int)dist_error - 1;
        i1 = (int)index_dict_notch;
      }

      if (index_dict_notch > ppg_peak2) {
        i2 = 0;
        k = 0;
      } else {
        i2 = (int)index_dict_notch - 1;
        k = (int)ppg_peak2;
      }

      max_slope(filtered_ppg, ecg_peak1, ecg_peak2, b_filtered_ecg);

      /*    old feature */
      the_first_point_idx_0 = c_detected_ecg_peaks->size[0] *
        c_detected_ecg_peaks->size[1];
      c_detected_ecg_peaks->size[0] = 1;
      c_detected_ecg_peaks->size[1] = r2->size[1];
      emxEnsureCapacity_boolean_T(c_detected_ecg_peaks, the_first_point_idx_0);
      loop_ub = r2->size[0] * r2->size[1];
      for (the_first_point_idx_0 = 0; the_first_point_idx_0 < loop_ub;
           the_first_point_idx_0++) {
        c_detected_ecg_peaks->data[the_first_point_idx_0] = (r2->
          data[the_first_point_idx_0] && r3->data[the_first_point_idx_0]);
      }

      eml_find(c_detected_ecg_peaks, r1);
      if (r1->size[1] != 0) {
        the_first_point_idx_0 = c_detected_ecg_peaks->size[0] *
          c_detected_ecg_peaks->size[1];
        c_detected_ecg_peaks->size[0] = 1;
        c_detected_ecg_peaks->size[1] = r4->size[1];
        emxEnsureCapacity_boolean_T(c_detected_ecg_peaks, the_first_point_idx_0);
        loop_ub = r4->size[0] * r4->size[1];
        for (the_first_point_idx_0 = 0; the_first_point_idx_0 < loop_ub;
             the_first_point_idx_0++) {
          c_detected_ecg_peaks->data[the_first_point_idx_0] = (r4->
            data[the_first_point_idx_0] && r5->data[the_first_point_idx_0]);
        }

        eml_find(c_detected_ecg_peaks, r1);
        if (r1->size[1] != 0) {
          the_first_point_idx_0 = c_detected_ecg_peaks->size[0] *
            c_detected_ecg_peaks->size[1];
          c_detected_ecg_peaks->size[0] = 1;
          c_detected_ecg_peaks->size[1] = r6->size[1];
          emxEnsureCapacity_boolean_T(c_detected_ecg_peaks,
            the_first_point_idx_0);
          loop_ub = r6->size[0] * r6->size[1];
          for (the_first_point_idx_0 = 0; the_first_point_idx_0 < loop_ub;
               the_first_point_idx_0++) {
            c_detected_ecg_peaks->data[the_first_point_idx_0] = (r6->
              data[the_first_point_idx_0] && r7->data[the_first_point_idx_0]);
          }

          eml_find(c_detected_ecg_peaks, r1);
          if (r1->size[1] != 0) {
            /*             %% GENERATE FEATURES */
            the_first_point_idx_0 = f_ai_values->size[0];
            n = f_ai_values->size[0];
            f_ai_values->size[0]++;
            emxEnsureCapacity_real_T(f_ai_values, n);
            idx = (int)ppg_peak1 - 1;
            f_ai_values->data[the_first_point_idx_0] = (filtered_ppg->data[idx]
              - filtered_ppg->data[(int)inflection_point(filtered_ppg, ppg_peak1,
              ecg_peak2) - 1]) / filtered_ppg->data[idx];
            the_first_point_idx_0 = the_first_point->size[0];
            n = the_first_point->size[0];
            the_first_point->size[0]++;
            emxEnsureCapacity_int32_T(the_first_point, n);
            the_first_point->data[the_first_point_idx_0] = ecg_peak1;
            the_first_point_idx_0 = the_second_point->size[0];
            n = the_second_point->size[0];
            the_second_point->size[0]++;
            emxEnsureCapacity_int32_T(the_second_point, n);
            the_second_point->data[the_first_point_idx_0] = ecg_peak2;
            the_first_point_idx_0 = f_hr_values->size[0];
            n = f_hr_values->size[0];
            f_hr_values->size[0]++;
            emxEnsureCapacity_real_T(f_hr_values, n);
            f_hr_values->data[the_first_point_idx_0] = 15360.0 / ((double)
              ecg_peak2 - (double)ecg_peak1);
            the_first_point_idx_0 = f_ptt_values->size[0];
            n = f_ptt_values->size[0];
            f_ptt_values->size[0] += b_filtered_ecg->size[0];
            emxEnsureCapacity_real_T(f_ptt_values, n);
            loop_ub = b_filtered_ecg->size[0];
            for (n = 0; n < loop_ub; n++) {
              f_ptt_values->data[the_first_point_idx_0 + n] =
                (b_filtered_ecg->data[n] - (double)ecg_peak1) / 256.0;
            }

            the_first_point_idx_0 = detected_ecg_peaks->size[0] *
              detected_ecg_peaks->size[1];
            detected_ecg_peaks->size[0] = 1;
            detected_ecg_peaks->size[1] = PPG_min_positions->size[1];
            emxEnsureCapacity_real_T(detected_ecg_peaks, the_first_point_idx_0);
            loop_ub = PPG_min_positions->size[0] * PPG_min_positions->size[1];
            for (the_first_point_idx_0 = 0; the_first_point_idx_0 < loop_ub;
                 the_first_point_idx_0++) {
              detected_ecg_peaks->data[the_first_point_idx_0] = (ppg_peak1 -
                PPG_min_positions->data[the_first_point_idx_0]) / 256.0;
            }

            if ((f_uptime_values->size[0] != 0) && (f_uptime_values->size[1] !=
                 0)) {
              idx = f_uptime_values->size[1];
            } else if (detected_ecg_peaks->size[1] != 0) {
              idx = detected_ecg_peaks->size[1];
            } else {
              idx = f_uptime_values->size[1];
              if (idx <= 0) {
                idx = 0;
              }
            }

            empty_non_axis_sizes = (idx == 0);
            if (empty_non_axis_sizes || ((f_uptime_values->size[0] != 0) &&
                 (f_uptime_values->size[1] != 0))) {
              loop_ub = f_uptime_values->size[0];
            } else {
              loop_ub = 0;
            }

            if (empty_non_axis_sizes || (detected_ecg_peaks->size[1] != 0)) {
              i3 = 1;
            } else {
              i3 = 0;
            }

            the_first_point_idx_0 = b_f_uptime_values->size[0] *
              b_f_uptime_values->size[1];
            b_f_uptime_values->size[0] = loop_ub + i3;
            b_f_uptime_values->size[1] = idx;
            emxEnsureCapacity_real_T(b_f_uptime_values, the_first_point_idx_0);
            for (the_first_point_idx_0 = 0; the_first_point_idx_0 < idx;
                 the_first_point_idx_0++) {
              for (n = 0; n < loop_ub; n++) {
                b_f_uptime_values->data[n + b_f_uptime_values->size[0] *
                  the_first_point_idx_0] = f_uptime_values->data[n + loop_ub *
                  the_first_point_idx_0];
              }
            }

            for (the_first_point_idx_0 = 0; the_first_point_idx_0 < idx;
                 the_first_point_idx_0++) {
              b_loop_ub = i3;
              for (n = 0; n < b_loop_ub; n++) {
                b_f_uptime_values->data[loop_ub + b_f_uptime_values->size[0] *
                  the_first_point_idx_0] = detected_ecg_peaks->data[i3 *
                  the_first_point_idx_0];
              }
            }

            the_first_point_idx_0 = f_uptime_values->size[0] *
              f_uptime_values->size[1];
            f_uptime_values->size[0] = b_f_uptime_values->size[0];
            f_uptime_values->size[1] = b_f_uptime_values->size[1];
            emxEnsureCapacity_real_T(f_uptime_values, the_first_point_idx_0);
            loop_ub = b_f_uptime_values->size[0] * b_f_uptime_values->size[1];
            for (the_first_point_idx_0 = 0; the_first_point_idx_0 < loop_ub;
                 the_first_point_idx_0++) {
              f_uptime_values->data[the_first_point_idx_0] =
                b_f_uptime_values->data[the_first_point_idx_0];
            }

            the_first_point_idx_0 = detected_ecg_peaks->size[0] *
              detected_ecg_peaks->size[1];
            detected_ecg_peaks->size[0] = 1;
            detected_ecg_peaks->size[1] = dict_notch->size[1];
            emxEnsureCapacity_real_T(detected_ecg_peaks, the_first_point_idx_0);
            loop_ub = dict_notch->size[0] * dict_notch->size[1];
            for (the_first_point_idx_0 = 0; the_first_point_idx_0 < loop_ub;
                 the_first_point_idx_0++) {
              detected_ecg_peaks->data[the_first_point_idx_0] =
                (dict_notch->data[the_first_point_idx_0] -
                 PPG_min_positions->data[the_first_point_idx_0]) / 256.0;
            }

            if ((f_syst_time_values->size[0] != 0) && (f_syst_time_values->size
                 [1] != 0)) {
              idx = f_syst_time_values->size[1];
            } else if (detected_ecg_peaks->size[1] != 0) {
              idx = detected_ecg_peaks->size[1];
            } else {
              idx = f_syst_time_values->size[1];
              if (idx <= 0) {
                idx = 0;
              }
            }

            empty_non_axis_sizes = (idx == 0);
            if (empty_non_axis_sizes || ((f_syst_time_values->size[0] != 0) &&
                 (f_syst_time_values->size[1] != 0))) {
              loop_ub = f_syst_time_values->size[0];
            } else {
              loop_ub = 0;
            }

            if (empty_non_axis_sizes || (detected_ecg_peaks->size[1] != 0)) {
              i3 = 1;
            } else {
              i3 = 0;
            }

            the_first_point_idx_0 = b_f_uptime_values->size[0] *
              b_f_uptime_values->size[1];
            b_f_uptime_values->size[0] = loop_ub + i3;
            b_f_uptime_values->size[1] = idx;
            emxEnsureCapacity_real_T(b_f_uptime_values, the_first_point_idx_0);
            for (the_first_point_idx_0 = 0; the_first_point_idx_0 < idx;
                 the_first_point_idx_0++) {
              for (n = 0; n < loop_ub; n++) {
                b_f_uptime_values->data[n + b_f_uptime_values->size[0] *
                  the_first_point_idx_0] = f_syst_time_values->data[n + loop_ub *
                  the_first_point_idx_0];
              }
            }

            for (the_first_point_idx_0 = 0; the_first_point_idx_0 < idx;
                 the_first_point_idx_0++) {
              b_loop_ub = i3;
              for (n = 0; n < b_loop_ub; n++) {
                b_f_uptime_values->data[loop_ub + b_f_uptime_values->size[0] *
                  the_first_point_idx_0] = detected_ecg_peaks->data[i3 *
                  the_first_point_idx_0];
              }
            }

            the_first_point_idx_0 = f_syst_time_values->size[0] *
              f_syst_time_values->size[1];
            f_syst_time_values->size[0] = b_f_uptime_values->size[0];
            f_syst_time_values->size[1] = b_f_uptime_values->size[1];
            emxEnsureCapacity_real_T(f_syst_time_values, the_first_point_idx_0);
            loop_ub = b_f_uptime_values->size[0] * b_f_uptime_values->size[1];
            for (the_first_point_idx_0 = 0; the_first_point_idx_0 < loop_ub;
                 the_first_point_idx_0++) {
              f_syst_time_values->data[the_first_point_idx_0] =
                b_f_uptime_values->data[the_first_point_idx_0];
            }

            loop_ub = i1 - i;
            i1 = b_filtered_ecg->size[0];
            b_filtered_ecg->size[0] = loop_ub;
            emxEnsureCapacity_real_T(b_filtered_ecg, i1);
            for (i1 = 0; i1 < loop_ub; i1++) {
              b_filtered_ecg->data[i1] = filtered_ppg->data[i + i1];
            }

            d = trapz(b_filtered_ecg);
            loop_ub = f_dv_values->size[0];
            i = b_filtered_ecg->size[0];
            b_filtered_ecg->size[0] = f_dv_values->size[0] + 1;
            emxEnsureCapacity_real_T(b_filtered_ecg, i);
            for (i = 0; i < loop_ub; i++) {
              b_filtered_ecg->data[i] = f_dv_values->data[i];
            }

            b_filtered_ecg->data[f_dv_values->size[0]] = d;
            i = f_dv_values->size[0];
            f_dv_values->size[0] = b_filtered_ecg->size[0];
            emxEnsureCapacity_real_T(f_dv_values, i);
            loop_ub = b_filtered_ecg->size[0];
            for (i = 0; i < loop_ub; i++) {
              f_dv_values->data[i] = b_filtered_ecg->data[i];
            }

            loop_ub = k - i2;
            i = b_filtered_ecg->size[0];
            b_filtered_ecg->size[0] = loop_ub;
            emxEnsureCapacity_real_T(b_filtered_ecg, i);
            for (i = 0; i < loop_ub; i++) {
              b_filtered_ecg->data[i] = filtered_ppg->data[i2 + i];
            }

            d = trapz(b_filtered_ecg);
            loop_ub = f_sv_values->size[0];
            i = b_filtered_ecg->size[0];
            b_filtered_ecg->size[0] = f_sv_values->size[0] + 1;
            emxEnsureCapacity_real_T(b_filtered_ecg, i);
            for (i = 0; i < loop_ub; i++) {
              b_filtered_ecg->data[i] = f_sv_values->data[i];
            }

            b_filtered_ecg->data[f_sv_values->size[0]] = d;
            i = f_sv_values->size[0];
            f_sv_values->size[0] = b_filtered_ecg->size[0];
            emxEnsureCapacity_real_T(f_sv_values, i);
            loop_ub = b_filtered_ecg->size[0];
            for (i = 0; i < loop_ub; i++) {
              f_sv_values->data[i] = b_filtered_ecg->data[i];
            }

            /*         %% SAVE THE FEATURES INTO 1 FILE FOR EACH SUBJECT */
            if ((f_syst_time_values->size[0] != 0) && (f_syst_time_values->size
                 [1] != 0)) {
              loop_ub = f_syst_time_values->size[1];
            } else {
              loop_ub = 0;
            }

            if ((f_uptime_values->size[0] != 0) && (f_uptime_values->size[1] !=
                 0)) {
              b_loop_ub = f_uptime_values->size[1];
            } else {
              b_loop_ub = 0;
            }

            idx = the_first_point->size[0];
            n = the_first_point->size[0];
            k = the_first_point->size[0];
            the_first_point_idx_0 = the_first_point->size[0];
            ecg_peak1 = the_first_point->size[0];
            ecg_peak2 = the_first_point->size[0];
            b_the_first_point_idx_0 = the_first_point->size[0];
            c_the_first_point_idx_0 = the_first_point->size[0];
            d_the_first_point_idx_0 = the_first_point->size[0];
            i = features->size[0] * features->size[1];
            features->size[0] = the_first_point->size[0];
            i1 = loop_ub + b_loop_ub;
            features->size[1] = i1 + 7;
            emxEnsureCapacity_real_T(features, i);
            for (i = 0; i < 1; i++) {
              for (i2 = 0; i2 < idx; i2++) {
                features->data[i2] = the_first_point->data[i2];
              }
            }

            for (i = 0; i < 1; i++) {
              for (i2 = 0; i2 < n; i2++) {
                features->data[i2 + features->size[0]] = the_second_point->
                  data[i2];
              }
            }

            for (i = 0; i < 1; i++) {
              for (i2 = 0; i2 < k; i2++) {
                features->data[i2 + features->size[0] * 2] = f_ptt_values->
                  data[i2];
              }
            }

            for (i = 0; i < 1; i++) {
              for (i2 = 0; i2 < the_first_point_idx_0; i2++) {
                features->data[i2 + features->size[0] * 3] = f_hr_values->
                  data[i2];
              }
            }

            for (i = 0; i < 1; i++) {
              for (i2 = 0; i2 < ecg_peak1; i2++) {
                features->data[i2 + features->size[0] * 4] = f_ai_values->
                  data[i2];
              }
            }

            for (i = 0; i < loop_ub; i++) {
              for (i2 = 0; i2 < ecg_peak2; i2++) {
                features->data[i2 + features->size[0] * (i + 5)] =
                  f_syst_time_values->data[i2 + ecg_peak2 * i];
              }
            }

            for (i = 0; i < b_loop_ub; i++) {
              for (i2 = 0; i2 < b_the_first_point_idx_0; i2++) {
                features->data[i2 + features->size[0] * ((i + loop_ub) + 5)] =
                  f_uptime_values->data[i2 + b_the_first_point_idx_0 * i];
              }
            }

            for (i = 0; i < 1; i++) {
              for (i2 = 0; i2 < c_the_first_point_idx_0; i2++) {
                features->data[i2 + features->size[0] * (i1 + 5)] =
                  f_sv_values->data[i2];
              }
            }

            for (i = 0; i < 1; i++) {
              for (i2 = 0; i2 < d_the_first_point_idx_0; i2++) {
                features->data[i2 + features->size[0] * (i1 + 6)] =
                  f_dv_values->data[i2];
              }
            }
          }
        }
      }
    }
  }

  emxFree_real_T(&b_f_uptime_values);
  emxFree_boolean_T(&c_detected_ecg_peaks);
  emxFree_int32_T(&r1);
  emxFree_boolean_T(&r7);
  emxFree_boolean_T(&r6);
  emxFree_boolean_T(&r5);
  emxFree_boolean_T(&r4);
  emxFree_boolean_T(&r3);
  emxFree_boolean_T(&r2);
  emxFree_real_T(&filtered_ppg);
  emxFree_real_T(&b_filtered_ecg);
  emxFree_real_T(&dict_notch);
  emxFree_real_T(&PPG_min_positions_after);
  emxFree_real_T(&PPG_min_positions);
  emxFree_real_T(&notch);
  emxFree_real_T(&foot);
  emxFree_real_T(&systolic_peak);
  emxFree_int32_T(&locs_ecg_peak);
  emxFree_real_T(&detected_ecg_peaks);
  emxFree_real_T(&f_sv_values);
  emxFree_real_T(&f_dv_values);
  emxFree_real_T(&f_syst_time_values);
  emxFree_real_T(&f_uptime_values);
  emxFree_real_T(&f_ai_values);
  emxFree_real_T(&f_ptt_values);
  emxFree_real_T(&f_hr_values);
  emxFree_int32_T(&the_second_point);
  emxFree_int32_T(&the_first_point);
}

/* End of code generation (extract.c) */
