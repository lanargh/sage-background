/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * trapz.c
 *
 * Code generation for function 'trapz'
 *
 */

/* Include files */
#include "trapz.h"
#include "extract.h"
#include "extract_emxutil.h"
#include "rt_nonfinite.h"

/* Function Definitions */
double trapz(const emxArray_real_T *x)
{
  double z;
  emxArray_real_T *c;
  int vlen;
  int loop_ub;
  int ix;
  double b_c;
  int ia;
  z = 0.0;
  if (x->size[0] > 1) {
    emxInit_real_T(&c, 1);
    vlen = c->size[0];
    c->size[0] = x->size[0];
    emxEnsureCapacity_real_T(c, vlen);
    loop_ub = x->size[0];
    for (vlen = 0; vlen < loop_ub; vlen++) {
      c->data[vlen] = 1.0;
    }

    c->data[0] = 0.5;
    c->data[x->size[0] - 1] = 0.5;
    vlen = x->size[0];
    for (loop_ub = 1; vlen < 0 ? loop_ub >= 1 : loop_ub <= 1; loop_ub += vlen) {
      ix = 0;
      b_c = 0.0;
      for (ia = 1; ia <= vlen; ia++) {
        b_c += x->data[ia - 1] * c->data[ix];
        ix++;
      }

      z += b_c;
    }

    emxFree_real_T(&c);
  }

  return z;
}

/* End of code generation (trapz.c) */
