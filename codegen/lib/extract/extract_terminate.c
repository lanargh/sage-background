/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * extract_terminate.c
 *
 * Code generation for function 'extract_terminate'
 *
 */

/* Include files */
#include "extract_terminate.h"
#include "extract.h"
#include "extract_data.h"
#include "rt_nonfinite.h"

/* Function Definitions */
void extract_terminate(void)
{
  omp_destroy_nest_lock(&emlrtNestLockGlobal);
  isInitialized_extract = false;
}

/* End of code generation (extract_terminate.c) */
