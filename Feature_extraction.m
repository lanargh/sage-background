clear; 
close all;
clc;

%% Choose file
[filename, pathname] = uigetfile('.psg', 'MultiSelect', 'on');

%% Decode
[ECG, PPG, Event]= ThothgetECGPPG(pathname, filename);
fs_ECG = ECG.fs;
ecg = ECG.Lead2;
fs_PPG = PPG.ACfs;
ppg = PPG.PPGACIR;
if fs_ECG == fs_PPG
    samplingrate = fs_ECG;
else
    disp('Different fs for the input signals')
end

% Sample cut 10-15 s
ecg = ecg(2560:3840);
ppg = ppg(2560:3840);

abp = zeros (size(ecg));
part_3_cells = [ppg'; abp'; ecg'];
file_name = sprintf('./Subject_0.mat');
save(file_name, 'part_3_cells','-v4') 

%% Plot the original data
figure;
subplot(211);
plot((1:length(ecg))/samplingrate, ecg);
title('ECG')
subplot(212);
plot((1:length(ppg))/samplingrate, ppg);
title('PPG');

%% Initialization
% Features
the_first_point = [];
the_second_point = [];

f_hr_values = [];
f_ptt_values = [];
f_ai_values = [];
f_uptime_values = [];
f_syst_time_values = [];
f_dv_values = [];
f_sv_values = [];

parameter_features = [];

% 3 groundtruth signals
gt_sbp_values = [];
gt_dbp_values = [];
gt_map_values = [];

%% Preprocessing
%   Smoothing
[envHigh, envLow] = envelope(ppg, 16, 'peak');
envMean = (envHigh + envLow)/2;

%Kalo mau liat envelope buat apa ini diuncomment aja
%figure
%plot(ppg); hold on; plot(envMean); hold on; plot(envHigh);
%legend('ppg', 'avg', 'envHigh');

%   Transform ecg & ppg into frequency spectrum - Remove the low
%   frequency components - Inverse to transform back to time domain
ecg_fftresult = fft(ecg);
ecg_fftresult(1 : round(length(ecg_fftresult)*1/samplingrate)) = 0;
ecg_fftresult(end - round(length(ecg_fftresult)*1/samplingrate) : end) = 0;
filtered_ecg = real(ifft(ecg_fftresult));

ppg_fftresult = fft(envMean);
ppg_fftresult(1 : round(length(ppg_fftresult) * 1/samplingrate)) = 0;
ppg_fftresult(end - round(length(ppg_fftresult) * 1/samplingrate) : end) = 0;
filtered_ppg = real(ifft(ppg_fftresult));

%figure; subplot(211); plot (ecg); hold on; plot(filtered_ecg); subplot(212); plot (ppg); hold on; plot(filtered_ppg);

%   Find peaks in ECG
win_size = floor(samplingrate * 571 / 1000);
if rem(win_size, 2) ==0
    win_size = win_size+1;
end
detected_ecg_peaks = ecgwindowedfilter(filtered_ecg, win_size);

%   Scale ecg
r_peaks = detected_ecg_peaks/(max(detected_ecg_peaks)/7);

%   Filter by threshold filter
% for data = 1:1:length(r_peaks)
%     if r_peaks(data) < 4 
%         r_peaks(data) = 0;
%     else
%         r_peaks(data)=1;
%     end
% end

locs_ecg_peak = find(r_peaks > 0);
if length(locs_ecg_peak) == 1
    disp('Insufficient number of peaks detected')
end

r_distance = locs_ecg_peak(2)-locs_ecg_peak(1);
if r_distance > 700
    disp('Insufficient distance between peaks detected')
end

%   Returns minimum distance between two peaks
for data = 1:1:length(locs_ecg_peak)-1
    if locs_ecg_peak(data+1)-locs_ecg_peak(data) < r_distance 
        r_distance = locs_ecg_peak(data+1)-locs_ecg_peak(data);
    end
end

filtered_ecg = filtered_ecg';
filtered_ppg = filtered_ppg';

%   Normalization (0,1)
filtered_ecg = (filtered_ecg - min(filtered_ecg));
normalized_ecg = filtered_ecg / max(filtered_ecg);
filtered_ppg = (filtered_ppg - min(filtered_ppg));
normalized_ppg = filtered_ppg / max(filtered_ppg);

%   Find peaks in PPG
[footIndex, systolicIndex, notchIndex, dicroticIndex ] = ...
ppgannotation(normalized_ppg, samplingrate, 1, 'other', 1);
systolic_peak = unique(systolicIndex)';
foot = unique(footIndex);
notch = unique(notchIndex);
dicrotic_peak = unique(dicroticIndex);

detected_ppg_peaks = zeros(size(normalized_ppg));
for i = 1:length(detected_ppg_peaks)
    if any(systolic_peak(:) == i)
        detected_ppg_peaks (i) = normalized_ppg(i);
    else
        detected_ppg_peaks (i) = 0;
    end
end
detected_ppg_peaks = detected_ppg_peaks';

figure; subplot (211); plot (normalized_ecg); hold on; plot(detected_ecg_peaks); subplot(212); plot (normalized_ppg); hold on; plot(detected_ppg_peaks);

if(length(locs_ecg_peak) > length(systolic_peak))
    loccc = length(systolic_peak);
else
    loccc = length(locs_ecg_peak);
end

fignum = 1;

%% Feature Extraction
for peak = 1:loccc-2
    if peak == loccc
        continue
    else
        ecg_peak1 = locs_ecg_peak(peak);
        ecg_peak2 = locs_ecg_peak(peak+1);
        ppg_peak1 = systolic_peak(peak);
        ppg_peak2 = systolic_peak(peak+1);

        dist_error = abs(ppg_peak2 - ecg_peak1);
        iiiii = systolic_peak >= ecg_peak1 & systolic_peak <= ecg_peak2;
        PPG_peak_find = systolic_peak(iiiii);

        if(dist_error > samplingrate*2)
            ecg_peak1 = locs_ecg_peak(peak+1);
            ecg_peak2 = locs_ecg_peak(peak+2);
        elseif (dist_error < samplingrate/2 && dist_error > 0)
            ppg_peak1 = systolic_peak(peak+1);
            ppg_peak2 = systolic_peak(peak+2);
        end

        part_ecg = normalized_ecg(ecg_peak1:ppg_peak2);
        part_abp = abp(ecg_peak1:ppg_peak2);
        part_ppg = normalized_ppg(ecg_peak1:ppg_peak2);

        part = normalized_ppg(ppg_peak1:ppg_peak2);

        ii = foot >= ecg_peak1 & foot <= ecg_peak2;
        iii = notch >= ecg_peak1 & notch <= ppg_peak2;
        iiii = foot >= ecg_peak1 & foot <= ppg_peak2;
                
        PPG_min_positions = foot(ii) + 1;
        PPG_min_positions_after = foot(iiii) + 1;

        if (length(PPG_min_positions_after) > 1)
            PPG_min_positions_after = max(PPG_min_positions_after);
        end
        if (length(PPG_min_positions) > 1)
            PPG_min_positions = max(PPG_min_positions);
        end
        dict_notch = notch(iii);

        if(length(dict_notch) > 1)
            dict_notch = max(dict_notch);
        elseif (length(dict_notch) < 1)
            disp (peak);
        end
        
        part_dv = normalized_ppg(PPG_min_positions:dict_notch);
        part_sv = normalized_ppg(dict_notch:PPG_min_positions_after);
        slope = max_slope(normalized_ppg,ecg_peak1,ecg_peak2);
        inf_post = inflection_point(normalized_ppg,ppg_peak1,ecg_peak2);

        %   old feature
        point_inflection = normalized_ppg(inf_post);
        point_max = normalized_ppg(ppg_peak1);
        b = point_max - point_inflection;
        a = point_max;
        
        if isempty(find(ii)) || isempty(find(iii)) || isempty(find(iiii)) ...
               || isempty(inf_post)
            continue
        end

        %% GENERATE FEATURES
        ai = b/a;
        f_ai_values = [f_ai_values;ai];

        the_first_point = [the_first_point;ecg_peak1];
        the_second_point = [the_second_point;ecg_peak2];

        hr = 60*samplingrate/(ecg_peak2-ecg_peak1);     
        f_hr_values = [f_hr_values;hr];

        ptt = (slope - ecg_peak1)/samplingrate;
        f_ptt_values = [f_ptt_values;ptt];

        up_time = (ppg_peak1 - PPG_min_positions)/samplingrate;
        f_uptime_values = [f_uptime_values; up_time];

        syst_time = (dict_notch - PPG_min_positions)/samplingrate;
        f_syst_time_values = [f_syst_time_values; syst_time];

        distance_dv = trapz(part_dv);                
        f_dv_values = [f_dv_values;distance_dv];

        distance_sv = trapz(part_sv);               
        f_sv_values = [f_sv_values;distance_sv];

        sbp = max(part_abp);
        gt_sbp_values = [gt_sbp_values;sbp];

        dbp = min(part_abp);
        gt_dbp_values = [gt_dbp_values;dbp];

        map = dbp + ((sbp-dbp)/3);
        gt_map_values = [gt_map_values;map];
    end

    %% SAVE THE FEATURES INTO 1 FILE FOR EACH SUBJECT
    parameter_features = [the_first_point the_second_point f_ptt_values f_hr_values f_ai_values f_syst_time_values f_uptime_values f_sv_values f_dv_values gt_sbp_values gt_dbp_values gt_map_values];
    file_name = sprintf('./Subject_new_0.mat');
    save(file_name,'parameter_features','-v4') ;
end