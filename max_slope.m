function op=max_slope(s,min_value,max_value)

sections1 = s(min_value:max_value);
differences = diff(sections1);
max_diffrences = max(differences);

% max_slope:
op=min_value+find(differences == max_diffrences);


